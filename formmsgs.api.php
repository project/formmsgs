<?php

/**
 * @file
 * Hooks provided by the Node module.
 */

/**
 * Provide messages for form elements.
 *
 * A formmsgs_element field object.
 *
 * A wildcard (*) can be given to match all forms.
 * Partial wildcards?
 */
function hook_formmsgs_elements() {
  
}

/**
 *
 */
function hook_formmsgs_element_alter($element_id) {

}
